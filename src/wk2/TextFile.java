package wk2;

public class TextFile extends DesktopItem {
    private String contents;

    public TextFile(String name) {
        super(name);
        contents = "";
    }

    @Override
    public void open() {
        System.out.println(contents);
    }

    @Override
    public int size() {
        return super.size() + contents.length();
    }

    public void add(String contents) {
        this.contents = contents;
    }

    public String replace(String contents) {
        String oldContents = contents;
        this.contents = contents;
        return oldContents;
    }
}
