# Announcements
 * Office hour/lunch 11am-noon in Spitzer Commons
 * Out of town tomorrow - watch videos and take notes tomorrow
 * Will be available 10am-11am and noon-1pm for additional office hours
 * Lab 2 and SLM 2 released, pre-work due before lab next Wednesday

# Java Fundamentals
 - [x] Be aware of the memory requirements and value ranges for primitive types
 - [x] Use increment and decrement operators
 - [x] Explain how pre- and post- increment/decrement operators differ in functionality
 - [x] Use mathematic operations to manipulate characters
 - [x] Interpret code in which automatic type conversions are present
 - [x] Use type casting to explicitly convert data types
 - [x] Explain the risks associated with explicit type casting
 - [x] Use short-circuit evaluation to avoid divide-by-zero and null-pointer exceptions

# Interfaces
 - [x] Explain the concept of the Java interface
 - [x] Make use of an interface reference to refer to objects that implement the interface