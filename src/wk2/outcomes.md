# Announcements
 * Team create for class
 * Lunch today at 11am, tomorrow at noon

# Inheritance

 - [x] Define aggregation
 - [x] Define composition
 - [x] Read and understand UML class diagrams
 - [x] Use inheritance in creating classes
 - [x] If no default constructor is present in the superclass, explain why a constructor of a subclass should make an explicit call to a constructor of the superclass
 - [x] Use aggregation and composition within user defined classes
 - [x] Explain what is meant by "overriding a method"
 - [x] Make use of super reference to call a parent method from within a method that overrides it

# UML
 - [x] Implement source code that meets the design specified in a UML class diagram