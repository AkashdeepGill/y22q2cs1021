package wk2;

public abstract class DesktopItem {

    public static void main(String[] args) {
        Folder folder = new Folder("folder");
        folder.add(new TextFile("file1.txt"));
        folder.add(new WavFile("moo.wav"));
        folder.add(new TextFile("file2.txt"));
        System.out.println(folder.size());
        folder.add(new TextFile("file2.txt"));
        System.out.println(folder.size());
    }

    private String name;

    public DesktopItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void open();

    public void rename(String name) {
        this.name = name;
    }

    public int size() {
        return name.length();
    }
}
