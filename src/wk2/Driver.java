package wk2;

public class Driver {
    public static void main(String[] args) {
        Parent parent = new Parent();
        Child child = new Child();
        parent.cry(1.0);
        child.cry(0.5);
        Parent step = child;
        System.out.println(parent);
    }
}
