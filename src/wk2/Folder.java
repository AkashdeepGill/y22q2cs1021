package wk2;

import java.util.ArrayList;
import java.util.List;

public class Folder extends DesktopItem implements Groupable {
    private List<DesktopItem> items;

    public Folder(String name) {
        super(name);
        items = new ArrayList<>();
    }

    @Override
    public void open() {
        System.out.println("Opening the folder called " + getName());
    }

    // TODO explain why this doesn't work before the end of the quarter
    @Override
    public boolean add(DesktopItem item) {
        boolean changed = false;
        if(!items.contains(item)) {
            items.add(item);
            changed = true;
        }
        return changed;
    }

    @Override
    public int size() {
        int size = super.size();
        for(DesktopItem item : items) {
            size += item.size();
        }
        return size;
    }
}
